// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyA99GB_amSPywG5pbtP5Nfhyk9CGKnirSk',
    authDomain: 'webkert-kotprog-f5707.firebaseapp.com',
    projectId: 'webkert-kotprog-f5707',
    storageBucket: 'webkert-kotprog-f5707.appspot.com',
    messagingSenderId: '1003949789140',
    appId: '1:1003949789140:web:da7402be37f6096f0e5041',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
