import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from 'src/app/Product';
import {
  DefaultProduct,
  ProductService,
} from 'src/app/services/product.service';

@Component({
  selector: 'app-product-page',
  templateUrl: './product-page.component.html',
  styleUrls: ['./product-page.component.css'],
})
export class ProductPageComponent implements OnInit {
  product: Product = DefaultProduct;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private productService: ProductService
  ) {}

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    let id = routeParams.get('id');

    if (!id) this.router.navigate(['/']);
    else {
      this.productService.getProduct(id).subscribe((product) => {
        this.product = product as Product;
      });
    }
  }
}
