import { Component, OnInit, Input } from '@angular/core';
import { Product } from 'src/app/Product';
import { faTimes, faEdit } from '@fortawesome/free-solid-svg-icons';
import { ProductService } from 'src/app/services/product.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.css'],
})
export class ProductItemComponent implements OnInit {
  @Input() product: Product;
  faTimes = faTimes;
  faEdit = faEdit;

  constructor(
    private productService: ProductService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {}

  deleteItem(prod: Product) {
    this.productService.deleteProduct(prod);
  }
  navigateToUpdate(prod: Product) {
    this.router.navigate([`/update-product/${prod.id}`]);
  }
  navigateToDetails(prod: Product) {
    this.router.navigate([`/product/${prod.id}`]);
  }
}
