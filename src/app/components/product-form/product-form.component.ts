import { Component, Input, OnInit } from '@angular/core';
import { Product, ProductStatusType } from 'src/app/Product';
import { ProductService } from 'src/app/services/product.service';
import { v4 as uuidv4 } from 'uuid';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css'],
})
export class ProductFormComponent implements OnInit {
  @Input() product: Product;

  name: string;
  href: string;
  desc: string;
  bundle: boolean = false;
  visible: boolean = false;
  serial: string;
  prodstatus: ProductStatusType;
  startDate: Date;
  orderDate: Date;
  terminationDate: Date;

  public status = enumSelector(ProductStatusType);

  constructor(private productService: ProductService) {}

  ngOnInit(): void {}

  saveName(value: any) {
    this.name = value.value;
  }
  saveHref(value: any) {
    this.href = value.value;
  }
  saveDescription(value: any) {
    this.desc = value.value;
  }
  saveBundle(value: any) {
    this.bundle = value.value;
  }
  saveVisible(value: any) {
    this.visible = value.value;
  }
  saveSerial(value: any) {
    this.serial = value.value;
  }
  saveStatus(value: any) {
    this.prodstatus = value.value;
  }
  saveStartDate(value: any) {
    this.startDate = value.value;
  }
  saveOrderDate(value: any) {
    this.orderDate = value.value;
  }
  saveTerminationDate(value: any) {
    this.terminationDate = value.value;
  }
  submit() {
    if (this.product) {
      const newProd: Product = {
        id: this.product.id,
        name: this.name,
        href: this.href,
        description: this.desc,
        isBundle: this.bundle,
        isCustomerVisible: this.visible,
        productSerialNumber: this.serial,
        status: this.prodstatus,
        startDate: this.startDate,
        orderDate: this.orderDate,
        terminationDate: this.terminationDate,
      };

      this.productService.updateProduct(newProd);
    } else {
      const newProd: Product = {
        id: uuidv4(),
        name: this.name,
        href: this.href,
        description: this.desc,
        isBundle: this.bundle,
        isCustomerVisible: this.visible,
        productSerialNumber: this.serial,
        status: this.prodstatus,
        startDate: this.startDate,
        orderDate: this.orderDate,
        terminationDate: this.terminationDate,
      };

      this.productService.addProduct(newProd);
    }
  }
}

function enumSelector(definition: any) {
  let newMap;
  newMap = Object.keys(definition).filter((k) => isNaN(Number(k)));
  return newMap;
}
