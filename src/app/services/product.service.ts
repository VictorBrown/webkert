import { Injectable } from '@angular/core';
import { Product, ProductStatusType } from '../Product';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument,
} from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  productsCollection: AngularFirestoreCollection<Product>;
  products: Observable<Product[]>;
  productDoc: AngularFirestoreDocument<Product>;
  product: Product;

  constructor(public afs: AngularFirestore) {
    //this.products = this.afs.collection('products').valueChanges();

    this.productsCollection = this.afs.collection('products', (ref) =>
      ref.orderBy('name', 'asc')
    );

    this.products = this.productsCollection.snapshotChanges().pipe(
      map((changes: any[]) => {
        return changes.map((a) => {
          const data = a.payload.doc.data() as Product;
          data.id = a.payload.doc.id;
          return data;
        });
      })
    );
  }

  getProducts() {
    return this.products;
  }

  getProduct(id: string) {
    return this.getProducts().pipe(
      map((products) => products.find((product) => product.id == id))
    );
  }

  addProduct(product: Product) {
    this.productsCollection.add(product);
  }

  deleteProduct(product: Product) {
    this.productDoc = this.afs.doc(`products/${product.id}`);
    this.productDoc.delete();
  }

  updateProduct(product: Product) {
    console.log(product);

    this.productDoc = this.afs.doc(`products/${product.id}`);
    this.productDoc.update(product);
  }
}

export const DefaultProduct: Product = {
  id: '',
  name: '',
  href: '',
  description: '',
  isBundle: false,
  isCustomerVisible: false,
  productSerialNumber: '',
  status: ProductStatusType.created,
  startDate: new Date(Date.now()),
  orderDate: new Date(Date.now()),
  terminationDate: new Date(Date.now()),
};
