export interface Product {
  id: string;
  href: string;
  description: string;
  isBundle: boolean;
  isCustomerVisible: boolean;
  name: string;
  orderDate: Date;
  productSerialNumber: string;
  startDate: Date;
  terminationDate: Date;
  status: ProductStatusType;
}

export enum ProductStatusType {
  created,
  pendingActive,
  cancelled,
  active,
  pendingTerminate,
  terminated,
  suspended,
  aborted,
}
